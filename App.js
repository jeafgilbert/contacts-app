import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Home from './screens/Home';
import Create from './screens/Create';
import Update from './screens/Update';

const navigationOptions = {
  headerTintColor: '#00a6e3',
  headerTitleStyle: { color: 'black' }
}

const AppNavigator = createStackNavigator({
  Home: {
    screen: Home,
    navigationOptions: navigationOptions
  },
  Create: {
    screen: Create,
    navigationOptions: navigationOptions
  },
  Update: {
    screen: Update,
    navigationOptions: navigationOptions
  }
}, {
  initialRouteName: 'Home'
});

export default createAppContainer(AppNavigator);
