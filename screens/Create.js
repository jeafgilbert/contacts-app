import React from 'react';
import { ScrollView, Alert, StyleSheet, TextInput, Text, Platform, View, TouchableOpacity, Image } from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import Spinner from 'react-native-loading-spinner-overlay';
import { Ionicons } from '@expo/vector-icons';

export default class Create extends React.Component {
  state = {
    spinner: false,
    photo: null
  };

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'New Contact',
      headerRight:(
        <TouchableOpacity onPress={navigation.getParam('testMethod')}>
          <Text style={{paddingVertical: 8, paddingLeft: 30, fontSize: 18, marginRight: 20, color: '#00a6e3'}}>Save</Text>
        </TouchableOpacity>
      )
    }
  }

  constructor(props) {
    super(props);

    this.state = {
      isNetworkError: false,
      isPosting: false,
      contact: {}
    }
  }

  showAlert(title, message) {
    this.setState({ spinner: false });
    
    setTimeout(() => {
      Alert.alert(title, message);
    }, 100);
  }

  getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
  }

  componentDidMount() {
    this.getPermissionAsync();
    this.props.navigation.setParams({ testMethod: () => { this.save() } });
  }

  pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
    });

    // console.log(result);

    if (!result.cancelled) {
      this.setState({ photo: result });
    }
  };

  createFormData = (photo, body) => {
    const data = new FormData();
  
    // console.log(photo);
    data.append("photo", {
      name: 'photo.png',
      type: photo.type,
      uri:
        Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "")
    });
  
    if (body) {
      Object.keys(body).forEach(key => {
        data.append(key, body[key]);
      });
    }
  
    // console.log(data);
    return data;
  };

  postContact(contact) {
    fetch("https://simple-contact-crud.herokuapp.com/contact", {
      method: "POST",
      body: JSON.stringify(contact)
    })
    .then(response => response.json())
    .then(responseJson => {
      // console.log(responseJson);

      if (responseJson.statusCode && responseJson.statusCode !== 200) {
        this.showAlert("Save Error", responseJson.message);
        return;
      }

      // Save Succeeded
      let onGoBack = this.props.navigation.getParam('onGoBack');
      onGoBack();

      this.props.navigation.goBack();
    })
    .catch((error) => {
      this.showAlert("Save Error", "Please try again later.");
      console.error(error);
    });
  }

  handleUploadPhoto = () => {
    // Upload Image
    fetch("http://jeafgilbert.com/contactsapp/saveimage.php", {
      method: "POST",
      headers: { 'Content-Type': 'multipart/form-data' },
      body: this.createFormData(this.state.photo, { action:'ADD' })
    })
    .then(response => {
      // console.log(response);
      if (!response.ok) {
        return {
          is_error: true,
          statusText: response.statusText
        }
      }
      
      return response.json();
    })
    .then(responseJson => {
      // console.log(responseJson);
      
      if (responseJson.is_error) {
        this.showAlert("Error on uploading photo: ", responseJson.statusText);
        return;
      }

      // Upload Image Succeeded
      let contact = {
        firstName: this.state.contact.firstName.trim(),
        lastName: this.state.contact.lastName.trim(),
        age: this.state.contact.age.trim(),
        photo: responseJson.uri
      };

      // Post to Heroku
      this.postContact(contact);
    })
    .catch((error) => {
      this.showAlert("Save Error", "Failed uploading image.");
      console.error(error);
    });
  };

  save = () => {
    // Validate form
    if (!this.state.contact.firstName) {
      Alert.alert('First name is required');
      return;
    }
    if (!this.state.contact.lastName) {
      Alert.alert('Last name is required');
      return;
    }
    if (!this.state.contact.age) {
      Alert.alert('Age is required');
      return;
    }

    this.setState({
      spinner: true
    });

    if (this.state.photo != null) {
      this.handleUploadPhoto();
    }
    else {
      let contact = {
        firstName: this.state.contact.firstName.trim(),
        lastName: this.state.contact.lastName.trim(),
        age: this.state.contact.age.trim(),
        photo: 'N/A'
      };

      this.postContact(contact);
    }
  }

  onChangeFirstName(value) {
    this.state.contact.firstName = value;
  }
  onChangeLastName(value) {
    this.state.contact.lastName = value;
  }
  onChangeAge(value) {
    this.state.contact.age = value;
  }
  
  render() {
    if (this.state.isNetworkError) {
      return (
        <View style={[styles.container, styles.allCenter]}>
          <Ionicons name="md-information-circle-outline" style={styles.networkErrorIcon} />
          <Text style={styles.networkErrorText}>Network error. Please check your internet connection.</Text>
        </View>
      )
    }
    else {
      return (
        <ScrollView style={styles.container}>
          <Spinner
            visible={this.state.spinner}
            textContent={'Saving...'}
            textStyle={styles.spinnerTextStyle}
          />
          <View style={styles.boxPhoto}>
            <TouchableOpacity style={styles.wrapperPhoto} onPress={this.pickImage}>
              <Image style={styles.photoImage} source={this.state.photo ? {uri: this.state.photo.uri} : require('../assets/profile-default.png')} />
            </TouchableOpacity>
          </View>
          <View style={[styles.boxForm]}>
            <View style={styles.formGroup}>
              <View style={styles.inputHeader}>
                <Text style={styles.headerText}>First Name</Text>
              </View>
              <View style={styles.inputControl}>
                <TextInput style={styles.inputText} onChangeText={text => this.onChangeFirstName(text)} />
              </View>
            </View>
            <View style={styles.formGroup}>
              <View style={styles.inputHeader}>
                <Text style={styles.headerText}>Last Name</Text>
              </View>
              <View style={styles.inputControl}>
                <TextInput style={styles.inputText} onChangeText={text => this.onChangeLastName(text)} />
              </View>
            </View>
            <View style={styles.formGroup}>
              <View style={styles.inputHeader}>
                <Text style={styles.headerText}>Age</Text>
              </View>
              <View style={styles.inputControl}>
                <TextInput style={styles.inputText} onChangeText={text => this.onChangeAge(text)} />
              </View>
            </View>
          </View>
          <View style={styles.spacerBottom}></View>
        </ScrollView>
      )
    }
  }
}

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#FFF'
  },
  headerButton: { 
    fontSize: 18,
    marginRight: 20
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: 20
  },
  allCenter: { 
    alignItems: 'center',
    justifyContent: 'center'
  },
  networkErrorIcon: {
    fontSize: 120,
    color: '#aaa'
  },
  networkErrorText: {
    marginVertical: 20,
    marginHorizontal: 50,
    fontSize: 24,
    color: '#aaa',
    textAlign: 'center'
  },

  boxForm: {
    paddingVertical: 20
  },
  formGroup: {
    marginVertical: 10
  },
  headerText: {
    marginBottom: 5,
    fontSize: 16,
    fontWeight: 'bold'
  },
  inputText: {
    height: 48,
    paddingVertical: 3,
    paddingHorizontal: 15,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#ced4da',
    fontSize: 18
  },
  boxPhoto: {
    marginTop: 50,
    alignItems: 'center',
    justifyContent: 'center'
  },
  wrapperPhoto: {
    width: 160,
    height: 160,
    borderRadius: 80,
    borderWidth: 1,
    borderColor: '#ced4da',
    overflow: 'hidden'
  },
  photoImage: {
    width: '100%',
    height: '100%'
  },
	spacerBottom: {
		flex: 1,
		height: 250
  }
});