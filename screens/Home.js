import React from 'react';
import { ActivityIndicator, Alert, StyleSheet, Text, View, TouchableOpacity, TouchableHighlight, Image } from 'react-native';
import { SwipeListView } from 'react-native-swipe-list-view';
import { Ionicons } from '@expo/vector-icons';

export default class Home extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
        title: 'Contacts',
        headerRight: (
          <TouchableOpacity onPress={() => { navigation.navigate('Create', { onGoBack: navigation.getParam('onGoBack') }) }}>
            <Ionicons name="md-person-add" size={28} style={{marginRight: 20, color:'#00a6e3'}} />
          </TouchableOpacity>
        )
      }
    }

  goToCreate(navigation) {
    console.log(navigation);
  }

  constructor(props) {
    super(props);

    this.state = {
      isNetworkError: false,
      isFirstLoad: true,
      isFetching: false,
      contactsData: []
    }
  }

  componentDidMount() {
    this.getContacts();
    this.props.navigation.setParams({ onGoBack: this.refresh });
  }

  getContacts() {
    fetch('https://simple-contact-crud.herokuapp.com/contact')
    .then(response => response.json())
    .then(responseJson => {
        this.setState({
          isFirstLoad: false,
          isFetching: false,
          contactsData: responseJson.data
        });
      })
    .catch((error) => {
      console.error(error);
    });
  }

  onRefresh() {
    this.setState({ isFetching: true }, function() { 
      this.getContacts();
    });
  }

  confirmDeleteContact(id, row, fullName) {
    Alert.alert(
      'Delete Contact',
      'Are you sure want to delete ' + fullName + '?',
      [{ 
          text: 'OK',
          onPress: () => this.deleteContact(id, row)
        },
        {
          text: 'Cancel',
          onPress: () => row.closeRow(),
          style: 'cancel',
        }]
    );
  }

  deleteContact(id, row) {
    row.closeRow();

    let newContactsData = this.state.contactsData.filter(val => {
      if (val.id === id) {
        return false;
      }
      return true;
    }).map(val => { return val; });

    let oldContactsData = [...this.state.contactsData];
    this.setState({
      contactsData: newContactsData
    });

    fetch('https://simple-contact-crud.herokuapp.com/contact/' + id, 
      { method: 'DELETE' }
    )
    .then(response => response.json())
    .then(responseJson => {
      // console.log(responseJson);

      if (responseJson.statusCode && responseJson.statusCode !== 200) {
        Alert.alert("Delete Error", responseJson.message);
        this.setState({
          contactsData: oldContactsData
        });

        return;
      }
      else if (responseJson.message == "contact unavailable") {
        Alert.alert("Delete Error", "API DELETE is buggy. Even though 'id' is correct, it always returns 'contact unavailable'. Please try again if there is API update.");
        this.setState({
          contactsData: oldContactsData
        });

        return;
      }
      
      // Succeeded
    })
    .catch((error) => {
      Alert.alert("Delete Error", "Please try again later.");
      this.setState({
        contactsData: oldContactsData
      });

      console.error(error);
    });
  }

  viewContact(id) {
    this.props.navigation.navigate('Update', { 
      id:id,
      onGoBack: this.refresh
    });
  }

  refresh = () => {
    this.getContacts();
  }
  
  render() {
    if (this.state.isNetworkError) {
      return (
        <View style={[styles.container, styles.allCenter]}>
          <Ionicons name="md-information-circle-outline" style={styles.networkErrorIcon} />
          <Text style={styles.networkErrorText}>Network error. Please check your internet connection.</Text>
        </View>
      )
    }
    else if (this.state.isFirstLoad) {
      return (
        <View style={styles.container}>
          <View style={{ padding:30 }}>
						<ActivityIndicator />
					</View>
        </View>
      )
    }
    else {
      let contacts = this.state.contactsData.map((val, key) => {
        return {
          val: val,
          key: String(key)
        }
      });
      
      return (
        <View style={styles.container}>
          <SwipeListView
            contentContainerStyle={{ paddingBottom: 20 }}
            onRefresh={() => this.onRefresh()}
            refreshing={this.state.isFetching}
            style={styles.boxList}
            useFlatList={true}
            data = {contacts}
            renderItem = {(rowData, rowMap) => (
              <TouchableHighlight  underlayColor='transparent' style={styles.rowFront} key={rowData.item.key}
              onPress={() => this.viewContact(rowData.item.val.id)}>
                <View style={styles.boxContact}>
                  <View style={styles.boxPhotoImage}>
                    <Image style={styles.photoImage} source={rowData.item.val.photo != 'N/A' ? {uri: rowData.item.val.photo} : require('../assets/profile-default.png')} />
                  </View>
                  <View style={styles.boxContactBlurb}>
                    <Text style={styles.nameText}>{rowData.item.val.firstName} {rowData.item.val.lastName}</Text>
                    <Text style={styles.ageText}>Age: {rowData.item.val.age}</Text>
                  </View>
                </View>
              </TouchableHighlight>
            )}
            renderHiddenItem = {(rowData, rowMap) => (
              <View style={styles.rowBack}>
                {/* <TouchableOpacity style={[styles.actionButton, styles.primaryButton]}>
                  <Text style={styles.buttonText}>Edit</Text>
                </TouchableOpacity> */}
                <TouchableOpacity style={[styles.actionButton, styles.dangerButton, styles.ml5]}
                  onPress={_ => this.confirmDeleteContact(rowData.item.val.id, rowMap[rowData.item.key], rowData.item.val.firstName + ' ' + rowData.item.val.lastName)}>
                  <Text style={styles.buttonText}>Delete</Text>
                </TouchableOpacity>
              </View>
            )}
            leftOpenValue={85}
            rightOpenValue={-100}
            disableRightSwipe={true}
          />
          <View style={styles.bottomSpacer}></View>
        </View>
      )
    }
  }
}

const styles = StyleSheet.create({
  ml5: {
    marginLeft: 5
  },
  rowFront: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    height: 64,
  },
  rowBack: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingHorizontal: 10,
    backgroundColor: '#fff'
  },
  actionButton: {
    width: 75,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 7
  },
  primaryButton: { 
    backgroundColor: '#00a6e3'
  },
  dangerButton: {
    backgroundColor: '#dc3545'
  },
  buttonText: {
    color: 'white',
    fontSize: 16
  },

  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  allCenter: { 
    alignItems: 'center',
    justifyContent: 'center'
  },
  networkErrorIcon: {
    fontSize: 120,
    color: '#aaa'
  },
  networkErrorText: {
    marginVertical: 20,
    marginHorizontal: 50,
    fontSize: 24,
    color: '#aaa',
    textAlign: 'center'
  },
  loadingText: {
    alignSelf: 'center',
    marginTop: 50,
    fontSize: 16
  },
  boxList: {
    flexGrow: 1,
    paddingVertical: 10
  },
  boxContact: {
    flexDirection: 'row',
    textAlign: 'left',
    paddingHorizontal: 20
  },
  boxPhotoImage: {
    width: 48,
    height: 48,
    marginRight: 15,
    borderRadius: 24,
    overflow: 'hidden'
  },
  photoImage: {
    width: '100%',
    height: '100%'
  },
  boxContactBlurb: {
    flexGrow: 1
  },
  nameText: {
    marginTop: 5,
    textAlign: 'left',
    fontSize: 16,
    fontWeight: 'bold'
  },
  ageText: {
    marginTop: 3,
    fontSize: 16,
    color: '#999'
  }
});